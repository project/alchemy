<?php
/**
 * @file * Admin include file. 
*/

/**
 * @file
 * Admin include file.
 */

/**
 * Displays the form for the standard settings tab.
 *
 * @return
 *   array A structured array for use with Forms API.
 */
function alchemy_admin_settings() {
 
  $apikey = variable_get('alchemy_apikey','');
  $form['alchemy_apikey'] = array(
    '#type' => 'textfield',
    '#title' => t('Alchemy API Key'), 
    '#default_value' => $apikey,
    '#description' => t('You need an API key to use the Alchemy API service. !alchemy_link.',
      array(
        '!alchemy_link' => l(t('Get an Alchemy API key here'), 'http://www.alchemyapi.com/api/register.html', array('attributes' => array('target', 'alchemy'))),
      )
    ),
  );
  
  return system_settings_form($form);
}
